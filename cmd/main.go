package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"mygo_projects/rest_with_ws/config"
	"mygo_projects/rest_with_ws/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()
	router := gin.Default()

	router.GET("/ping", getPing)
	router.POST("/post1", getPost1)
	router.POST("/call1", callWs1)
	router.Use(CORSMiddleware())
	router.Run(cfg.HTTPPort)
}

func getPing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "pong",
	})
}

func getPost1(c *gin.Context) {
	var request models.Request
	err := c.ShouldBindJSON(&request)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, models.Response{
			ErrorCode: -1,
			ErrorMsg:  "wrong JSON fromat",
		})
		return
	}

	c.JSON(http.StatusCreated, models.Response{
		ErrorCode: 0,
		ErrorMsg:  "Success",
	})
	return
}

func callWs1(c *gin.Context) {
	var request models.Request
	err := c.ShouldBindJSON(&request)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, models.Response{
			ErrorCode: -1,
			ErrorMsg:  "wrong JSON fromat",
		})
		return
	}
	resp, err := requestWs(request)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, models.Response{
			ErrorCode: -1,
			ErrorMsg:  err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)
	return
}

func requestWs(req models.Request) (models.Response, error) {
	var res models.Response
	sb, err := json.Marshal(req)
	fmt.Println(string(sb))
	if err != nil {
		return res, err
	}
	reader := bytes.NewReader(sb)

	resp, err := http.Post("http://localhost:8182/post1", "application/json; charset=UTF-8", reader)
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}

	err = json.Unmarshal(body, &res)
	if err != nil {
		return res, errors.New("The Response of Bus is Wrong ")
	}

	return res, nil
}

//CORSMiddleware ...
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
