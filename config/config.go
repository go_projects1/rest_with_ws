package config

import "mygo_projects/rest_with_ws/models"

func Load() models.Config {
	config := models.Config{}

	//config.LogLevel = cast.ToString(getOrReturnDefaultValue("LOG_LEVEL", "debug"))
	//config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":7077"))

	config.LogLevel = "debug"
	config.HTTPPort = ":9192"
	return config
}
