package models

// Config model
type Config struct {
	LogLevel string
	HTTPPort string
}
