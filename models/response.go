package models

type Response struct {
	ErrorCode int    `json:"error_code"`
	ErrorMsg  string `json:"error_message"`
}
